﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using SimpleJSON;

public class DialogManager : MonoBehaviour
{
   [SerializeField] private float typingSpeed = 0.05f;

   [SerializeField] private TextMeshProUGUI chat;
    [SerializeField] private string[] kalimat;

    private int index;

    private void Start()
	{
        WWW link = new WWW("https://5e510330f2c0d300147c034c.mockapi.io/users");
       
        StartCoroutine(Kalimat(link));
       
    }
  

    DataOrang Orang(string json)
    {
        JSONArray nma = JSON.Parse(json).AsArray;

        return new DataOrang(nma[15]["name"], nma[15]["email"]);
    }

    private IEnumerator Kalimat(WWW kata)
    {
        yield return kata;


        DataOrang list = Orang(kata.text);
        foreach (char letter in kalimat[index].ToCharArray())
        {

            chat.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }
        chat.text += $"{list.name}. ";

        foreach (char letter in kalimat[index + 1].ToCharArray())
        {

            chat.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }
        chat.text += $" <b>{list.email}</b>.";

    }


}

public class DataOrang
{
    public string name;
    public string email;

    public DataOrang(string name, string email)
	{
        this.name = name;
        this.email = email;
	}

}