# Chat Bubble with JSON

**Muhammad Farhan Irfani**
**4210181015**

## Preview
![demonstration](/bubble.gif)

## Get JSON file from URL
```
private void Start()
	{
        WWW link = new WWW("https://5e510330f2c0d300147c034c.mockapi.io/users");
       
        StartCoroutine(Kalimat(link));
       
    }
```


## Get name and email from JSON
```
DataOrang Orang(string json)
    {
        JSONArray nma = JSON.Parse(json).AsArray;

        return new DataOrang(nma[15]["name"], nma[15]["email"]);
    }
```


## Coroutine to handle JSON and showing output
```
private IEnumerator Kalimat(WWW kata)
    {
        yield return kata;


        DataOrang list = Orang(kata.text);
        foreach (char letter in kalimat[index].ToCharArray())
        {

            chat.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }
        chat.text += $"{list.name}. ";

        foreach (char letter in kalimat[index + 1].ToCharArray())
        {

            chat.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }
        chat.text += $" <b>{list.email}</b>.";

    }
```


## Class that save data from JSON
```
public class DataOrang
{
    public string name;
    public string email;

    public DataOrang(string name, string email)
	{
        this.name = name;
        this.email = email;
	}

}
```

